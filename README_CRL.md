# react-abstract-menu

> An all-use-case menu component built with create-react-library

[![NPM](https://img.shields.io/npm/v/react-abstract-menu.svg)](https://www.npmjs.com/package/react-abstract-menu) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-abstract-menu
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'react-abstract-menu'
import 'react-abstract-menu/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Komefumi](https://github.com/Komefumi)
