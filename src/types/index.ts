import { FC as ReactFC } from "react";

export type ClassName = string;
export type LinkLocation = string;

export interface NavConfig {
  link: LinkLocation;
  exact: boolean;
  activeClassName?: ClassName;
}

export interface IMenuItem {
  label: string;
  value?: any;
  IconRendering?: ReactFC<any>;
  navConfig?: NavConfig;
}

export type SelectionSignallerType = (value: any) => void;

export enum MenuType {
  DISPLAY_TYPE = "display_type",
  CLICK_TYPE = "click_type",
  NAVIGATE_TYPE = "navigate_type",
}

export interface PMenuItem {
  menuItem: IMenuItem;
  onSelect?: SelectionSignallerType;
  className?: ClassName;
  menuType: MenuType;
}

export interface PMenu {
  className?: ClassName;
  listClassName?: ClassName;
  listItemClassName?: ClassName;
  menuItems: IMenuItem[];
  onSelect?: SelectionSignallerType;
  direction: MenuOrientation;
  menuType: MenuType;
}

export enum MenuOrientation {
  VERTICAL = "vertical",
  HORIZONTAL = "horizontal",
}
