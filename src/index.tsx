import React from "react";
import { NavLink } from "react-router-dom";

import {
  ClassName,
  IMenuItem,
  MenuOrientation,
  SelectionSignallerType,
  MenuType,
  PMenuItem,
  PMenu,
} from "./types";
import { InvalidArgumentError } from "helpful-exceptions";
import classes from "./classes.module.css";

export {
  ClassName,
  IMenuItem,
  MenuOrientation,
  SelectionSignallerType,
  MenuType,
  PMenuItem,
  PMenu,
};

const MenuItem = ({ menuItem, onSelect, className, menuType }: PMenuItem) => {
  const { label, value, IconRendering, navConfig } = menuItem;
  const selectionHandler = onSelect
    ? () => {
        onSelect(value);
      }
    : () => {};

  switch (menuType) {
    case MenuType.CLICK_TYPE:
      return (
        <li
          onClick={selectionHandler}
          className={`${classes.menuItem} ${className || ""}`}
        >
          <div className={classes.menuLabel}>{label}</div>
          {IconRendering && (
            <div className={classes.iconRendering}>
              <IconRendering />
            </div>
          )}
        </li>
      );
    case MenuType.DISPLAY_TYPE:
      return (
        <li className={`${classes.menuItem} ${className || ""}`}>
          <div className={classes.menuLabel}>{label}</div>
          {IconRendering && (
            <div className={classes.iconRendering}>
              <IconRendering />
            </div>
          )}
        </li>
      );
    case MenuType.NAVIGATE_TYPE:
      if (!navConfig) {
        throw new InvalidArgumentError(
          "When MenuType is of a navigable link list, a valid navConfig must be provided.",
        );
      }
      return (
        <li className={`${classes.menuItem} ${className || ""}`}>
          <NavLink to={navConfig.link} exact={navConfig.exact}>
            <div className={classes.menuLabel}>{label}</div>
            {IconRendering && (
              <div className={classes.iconRendering}>
                <IconRendering />
              </div>
            )}
          </NavLink>
        </li>
      );
    default:
      throw new InvalidArgumentError("Must provide a valid MenuType value");
  }
};

const Menu = ({
  className,
  listClassName,
  listItemClassName,
  menuItems,
  onSelect,
  direction,
  menuType,
}: PMenu) => {
  const containerClassName = `${className || ""} ${classes.container || ""}`;
  const menuClassName = `
    ${listClassName || ""}
    ${direction === MenuOrientation.VERTICAL ? classes.menuVertical : ""}
    ${direction === MenuOrientation.HORIZONTAL ? classes.menuHorizontal : ""}
    ${classes.menu}
  `;
  return (
    <div className={containerClassName}>
      <ul className={menuClassName}>
        {menuItems.map((currentItem) => (
          <MenuItem
            key={currentItem.value}
            menuItem={currentItem}
            onSelect={onSelect}
            className={listItemClassName}
            menuType={menuType}
          />
        ))}
      </ul>
    </div>
  );
};

export default Menu;
