# React Abstract Menu

Reusable, all purpose React Menu Component built with TypeScript. Feel free to use the source code for anything you see fit, it's MIT licensed.