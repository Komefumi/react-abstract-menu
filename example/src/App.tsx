import React from 'react'

import Menu from 'react-abstract-menu'
import 'react-abstract-menu/dist/index.css'

const App = () => {
  return <Menu />
}

export default App
